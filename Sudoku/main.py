import numpy as np

def parse_sudoku_lines(number_sudoku: int, lines_sudoku: list) -> list:
    sudokus = []
    for i in range(0, number_sudoku):
        sudoku = []
        sudoku_str = lines_sudoku[i*9:(i+1)*9]
        for j in range(0, 9):
            sudoku.append(list(map(int, sudoku_str[j].split())))
        sudokus.append(sudoku)
    return np.array(sudokus)


def parse_sudoku_input(filepath: str) -> (int, list):
    sudokus = []
    with open(filepath, "r") as f:
        lines = f.readlines()
        number_sudoku = int(lines[0])
        lines_sudoku = lines[1:]
        sudokus = parse_sudoku_lines(number_sudoku, lines_sudoku)
    return number_sudoku, sudokus

def parse_sudoku_output(filepath: str,number_sudoku: int) -> list:
    sudokus = []
    with open(filepath, "r") as f:
        lines = f.readlines()
        sudokus = parse_sudoku_lines(number_sudoku, lines)
    return sudokus
            

def execute_test(number_of_tests):
    for num_test in range(number_of_tests):
        input_filename = f"input/input0{num_test}.txt"
        number_sudoku, input_sudokus = parse_sudoku_input(input_filename)
        
        output_filename = f"output/output0{num_test}.txt"
        output_sudokus = parse_sudoku_output(output_filename, number_sudoku)
        
        for i in range(number_sudoku):
            filled_sudoku = fill_sudoku(input_sudokus[i])
            res = np.array_equal(filled_sudoku, output_sudokus[i])
            if res:
                print(f"Test {num_test} Sudoku {i} SUCCESS")
            else:
                print(f"Test {num_test} Sudoku {i} FAIL")


def fill_sudoku(sudoku: list) -> list:
    return sudoku







execute_test(1)
