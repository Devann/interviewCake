### ENONCE
# Write a function for finding the index of the "rotation point" which is 
# where I started working from the beginning of the dictionary. This list 
# is huge (there are lots of words I don't know) so we want to be efficient here.

### CODE
def find_rotation_point(words):
    floor = 0
    ceiling = len(words)
    rotation_point=words[floor]
    # Find the rotation point in the list
    while (ceiling-floor) > 1 :
        distance = (ceiling-floor)//2
        mid_indice = distance+floor
        mid_word = words[mid_indice]
        print(mid_indice,mid_word, rotation_point)
        if rotation_point < mid_word:
            floor = mid_indice
        elif rotation_point > mid_word:
            ceiling = mid_indice
        elif rotation_point == mid_word:
            break
        rotation_point=mid_word
    return mid_indice


### TESTS
import unittest

class Test(unittest.TestCase):

    def test_small_list(self):
        actual = find_rotation_point(['cape', 'cake'])
        expected = 1
        self.assertEqual(actual, expected)

    def test_medium_list(self):
        actual = find_rotation_point(['grape', 'orange', 'plum',
                                      'radish', 'apple'])
        expected = 4
        self.assertEqual(actual, expected)

    def test_large_list(self):
        actual = find_rotation_point(['ptolemaic', 'retrograde', 'supplant',
                                      'undulate', 'xenoepist', 'asymptote',
                                      'babka', 'banoffee', 'engender',
                                      'karpatka', 'othellolagkage'])
        expected = 5
        self.assertEqual(actual, expected)

    # Are we missing any edge cases?


unittest.main(verbosity=2)